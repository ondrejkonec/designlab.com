---
name: Logomark
---

## Tanuki

The logomark personifies GitLab’s values, culture, and stewardship of open source, taking inspiration from the tanuki – a raccoon dog native to Japan.

The tanuki is known for being fast, efficient, and collaborative. It is thought to symbolize trustworthiness, agility, and preparedness. Known for its transformational powers and collaborative nature, the tanuki works with others to achieve a common goal – a behavior that is also embodied by GitLab’s mission that everyone can contribute.

The tanuki logomark can be used when the core logo has already been presented to provide context. It should never be obscured or hard to recognize.

<figure-img label="Tanuki logomark sizing" src="/img/brand/tanuki-scalability.svg" width="720"></figure-img>

## Construction

DevOps is at our core, and the logomark represents adaptability, iteration, development, and – like the DevOps loop – momentum.

The construction of the GitLab tanuki was based on an infinity loop, representing the limitless possibilities of The DevOps Platform.

<figure-img label="Logomark construction" src="/img/brand/tanuki-construction.svg" width="480"></figure-img>

## Clear space

Like the logo, the clear space for the tanuki logomark is measured by the x-height, which equals the height of the letter "a" from the wordmark.

Ensuring proper clear space between the logomark and surrounding elements preserves the visual integrity of our brand; this area should be kept free of any visual elements, including text, graphics, borders, patterns, and other logos.

<figure-img label="Clear space around the logomark equal to the lowercase 'a' from the wordmark" src="/img/brand/tanuki-clearspace.svg" width="480"></figure-img>

## Incorrect usage

<figure-img label="Logomark incorrect usage" src="/img/brand/tanuki-incorrect-usage.svg"></figure-img>

1. Do not apply a stroke to the mark
1. Do not apply color to the mark
1. Do not outline or create a keyline around the mark
1. Do not flip or rotate the mark in any way
1. Do not apply drop shadow or effects to the mark
1. Do not add any other graphic elements to the mark
1. Do not distort the mark in any way
1. Do not use the mark as a framing device for imagery
