---
name: Photography
---

[Photography](https://drive.google.com/drive/folders/1VHErs-KSNX1FIIVgXJR3OmIzwU7M4E1M?usp=sharing) is how we frame DevOps in the real world. It is a useful tool for brand storytelling that allows users to see our product in action. It is important to keep visual consistency among all images, whether selecting stock photography or curating photos in-house. Images should be crisp with true-to-life colors and a natural editing style. When featuring subjects, include people from diverse backgrounds, ages, body types, races, and genders.

## Customer and contributor portraits

<figure-img alt="Portraits of smiling individuals" label="Customer portrait samples" src="/img/brand/adobe-stock-portrait-samples.jpg"></figure-img>

Customers and contributors are our heroes, so their portraits should feel heroic. We capture this by photographing people at eye-level on a neutral background with soft, natural lighting. A shallow depth of field minimizes background distractions and draws the viewer’s attention to the person.

## Metaphors

<figure-img alt="Aerial views that create abstract patterns and representations" label="Metaphor photo samples" src="/img/brand/stock-metaphor-samples.jpg"></figure-img>

Metaphorical photography allows us to convey intricate ideas in an abstract way. These photos are primarily shot top-down, with a focus on texture, patterns, or paths. The imagery should bring feelings of innovation, sustainability, positivity, and/or repetition. Images featuring infrastructure, people, nature, and structural repetition are excellent starting points.

## Collaboration

<figure-img alt="Overhead views of people collaborating around various devices and technology" label="Collaboration photo samples" src="/img/brand/stock-collaboration-samples.jpg"></figure-img>

Collaboration is a common theme at GitLab, as it highlights our mission that _everyone can contribute_. Capture collaborative scenes top-down or over a subject’s shoulder to show their working environment in a positive light. Subjects should be using their devices in a natural, candid way to show GitLab and DevOps as a normal part of everyday life. This technique draws attention to the working environment, rather than to the individual.
